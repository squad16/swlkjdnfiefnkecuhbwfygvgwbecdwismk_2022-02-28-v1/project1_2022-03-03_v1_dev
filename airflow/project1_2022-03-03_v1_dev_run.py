#!/usr/bin/env python3
import json
from time import sleep
import os


class AirflowRunError(RuntimeError):

    def __init__(self, *args: object) -> None:
        super().__init__(*args)


TERMINATE_STATES = {'success', 'failed'}
SINGLECONNECTOR_API_URL = "https://pam.megafon.ru/sc-aapm-ui/rest/aapm/password?passwordExpirationInMinute=30&passwordChangeRequired=FALSE&comment=getSecretsForTest&token={token}&responseType=JSON"
AIRFLOW_HOST_PURE = 'https://it-bigdata-prod.megafon.ru/airflow2/prod'
AIRFLOW_HOST = '{}/api/v1'.format(AIRFLOW_HOST_PURE)

import requests
from requests.auth import HTTPBasicAuth


def get_airflow_credentials(rest_url):
    if 'HIVE_TOKEN' not in os.environ:
        raise AirflowRunError("Token not found in your environment.")

    token_variable = os.environ.get('HIVE_TOKEN')
    url = rest_url.format(token=token_variable)

    responce = requests.get(url,
                            headers={'Cache-Control': 'no-cache'}, verify=False).json()["secret"]["data"]

    if 'login' in responce and 'password' in responce:
        result = json.loads(responce)
        return result['login'], result['password']
    else:
        raise AirflowRunError(
            "Login or password not found in singleconnector responce.")


def submit_dag(creds, dag_id, conf):
    return requests.post("{host}/dags/{dag_id}/dagRuns".format(host=creds['host'], dag_id=dag_id),
                         headers={'Cache-Control': 'no-cache', 'Content-Type': 'application/json'},
                         data=json.dumps(conf),
                         auth=HTTPBasicAuth(creds['login'], creds['password']),
                         verify=False).json()


def get_state(creds, dag_id, dag_run_id):
    return requests.get(
        "{host}/dags/{dag_id}/dagRuns/{dag_run_id}".format(host=creds['host'], dag_id=dag_id, dag_run_id=dag_run_id),
        headers={'Cache-Control': 'no-cache', 'Content-Type': 'application/json'},
        auth=HTTPBasicAuth(creds['login'], creds['password']),
        verify=False).json()


def main(snap_date_str):
    env_vars = os.environ
    splt = snap_date_str.replace('-', '')
    date_norm = '{year}-{month}-{day}'.format(year=splt[0:4], month=splt[4:6], day=splt[6:])
    print('Submitting project1_2022-03-03_v1_dev dag for date {}'.format(snap_date))
    poll_interval = int(env_vars.get('AIRFLOW_POLL_INTERVAL', 10))
    airflow_host = env_vars.get('AIRFLOW_HOST', AIRFLOW_HOST)

    dag_id = 'project1_2022-03-03_v1_dev'

    airflow_login, airflow_password = get_airflow_credentials(SINGLECONNECTOR_API_URL)

    creds = dict(
        host=airflow_host,
        login=airflow_login,
        password=airflow_password
    )

    parsed_resp = submit_dag(creds, dag_id, {'conf': {'snap_date': date_norm}})

    if 'dag_run_id' not in parsed_resp.keys():
        raise AirflowRunError(
            "Error while running dag. Try to check your credentials and airflow 2.0 REST API availablity")

    dag_run_id = parsed_resp['dag_run_id']

    state = get_state(creds, dag_id, dag_run_id)['state']
    while state not in TERMINATE_STATES:
        sleep(poll_interval)
        state = get_state(creds, dag_id, dag_run_id)['state']
    if state == 'failed':
        raise AirflowRunError('Error while running dag with id [{dag_id}] and execution_date [{execution_date}] '
                              'and dag_run_id [{dag_run_id}], see log at graylog'
                              '{airflow_host}/tree?dag_id={dag_id}'.format(
            dag_id=dag_id,
            execution_date=parsed_resp['execution_date'],
            dag_run_id=dag_run_id,
            airflow_host=AIRFLOW_HOST_PURE
        ))


if __name__ == '__main__':
    import sys

    args = sys.argv
    if len(args) != 2:
        raise AirflowRunError('Length of arguments should be equal 1')
    snap_date = args[1]
    main(snap_date)
